import time 
import visa
class MonoGPIBComms: 
    def __init__(self, VISAStr): 
        self.waveStr = "GOWAVE " 
        self.waveQueryStr = "WAVE?" 
        self.filtStr = "FILTER " 
        self.filtQueryStr = "FILTER?" 
        self.gratingStr = "GRAT " 
        self.gratingQueryStr = "GRAT?" 
        self.shutterStr = "SHUTTER " 
        self.gratingQueryStr = "SHUTTER?" 
        self.filterTransitionWavelengths = [] 
 
        self.visaStr = VISAStr 
        self.rm = visa.ResourceManager()    
        self.rm.list_resources() 
        print(self.rm) 
        print(self.visaStr) 
        self.inst = self.rm.open_resource(self.visaStr) 
        self.inst.timeout = 100000 
        self.inst.read_termination = "\r\n" 
        self.inst.write_termination = "\n" 
        self.inst.write("HANDSHAKE 0") 
        time.sleep(1) 
        getWaveStr = self.inst.query(self.waveQueryStr) 
        print("Monochromator initial wavelength set to " + getWaveStr + "nm")   
        self.waveLength = float(getWaveStr) 
        self.filtFile = open("FilterFile.FLT", "r") 
 
        # note, do not use the *IDN? with the PyVISA read or query functions 
        # it will not complete due to the monochromator not terminating the string 
        # properly. 
 
        # load the filter file to automatically change filters. 
        for line in self.filtFile.readlines(): 
            self.filterTransitionWavelengths.append(float(line))        
 
 
    def set_wavelength(self,wavelength): 
        self.waveLengthStr = wavelength#"{:.3f}".format(wavelength) 
        self.waveLength =  float(wavelength) 
        #print(self.waveLengthStr) 
        self.inst.write(self.waveStr + self.waveLengthStr) 
        time.sleep(0.010) 
        getWaveStr = self.inst.query(self.waveQueryStr) 
        self.set_filter() 
        return getWaveStr 
 
    def set_filter(self):           
        if self.waveLength < self.filterTransitionWavelengths[0]:       
            filtStrArg = "1"        
            self.inst.write(self.filtStr + filtStrArg) 
        elif self.filterTransitionWavelengths[0] <= self.waveLength < self.filterTransitionWavelengths[1]: 
            filtStrArg = "2"        
            self.inst.write(self.filtStr + filtStrArg)      
        elif self.filterTransitionWavelengths[1] <= self.waveLength < self.filterTransitionWavelengths[2]: 
            filtStrArg = "3"        
            self.inst.write(self.filtStr + filtStrArg)      
        elif self.filterTransitionWavelengths[2] <= self.waveLength < self.filterTransitionWavelengths[3]: 
            filtStrArg = "4"        
            self.inst.write(self.filtStr + filtStrArg) 
        elif self.filterTransitionWavelengths[3] <= self.waveLength < self.filterTransitionWavelengths[4]: 
            filtStrArg = "5"        
            self.inst.write(self.filtStr + filtStrArg) 
        elif self.filterTransitionWavelengths[4] <= self.waveLength < self.filterTransitionWavelengths[5]: 
            filtStrArg = "6"        
            self.inst.write(self.filtStr + filtStrArg)  
 
        time.sleep(0.010) 
        getFiltStr = self.inst.query(self.filtQueryStr) 
    
        return getFiltStr 
 
    def set_shutter(self,isShut): 
        self.isShut = bool(isShut) 
        if self.isShut: 
            self.setShutterStr = 'C' 
        else: 
            self.setShutterStr = 'O' 
 
        self.inst.write(self.shutterStr + self.setShutterStr) 
        time.sleep(0.010) 
        getShutterStr = self.inst.query(self.gratingQueryStr) 
        return getShutterStr 
 
    def set_grating(self,gratingNumber): 
        self.gratingNumber = gratingNumber          
        self.inst.write(self.gratingStr + self.gratingNumber) 
        time.sleep(0.010) 
        getGratStr = self.inst.query(self.gratingQueryStr)  
        return getGratStr 
 
    def close(self): 
        self.inst.close() 
        self.rm.close() 