"""For communicating with the Optical Power Meter in the Scatterometer Lab"""
from time import sleep
import numpy as np
import serial
import serial.tools.list_ports as pyserial_list_ports
class OpticalPowerMeter:
    """Enables use of Optical Power Meter in the Scatterometer Lab"""
    def __init__(self,
                 dummy: bool = False,
                 wavelength: int = 450,
                 port: str = ''):
        self.dummy = dummy
        self.serial_detail_description = "USB Serial Port (COM4)"
        self.serial_detail_device = "COM4" # This value may change
        self.serial_detail_hwid = "USB VID:PID=0403:6001 SER=AD02DXN1A"
        self.serial_detail_interface = "None"
        self.serial_detail_location = "None"
        self.serial_detail_manufacturer = "FTDI"
        self.serial_detail_name = "None"
        self.serial_detail_pid = "24577"
        self.serial_detail_product = "None"
        self.serial_detail_serial_number = "AD02DXN1A"
        self.optical_readings = []
        # self.inbound_queue              = inbound_queue
        # self.outbound_queue             = outbound_queue
        self.wavelength = wavelength
        
        if not self.dummy:
            if not port:
                self.determine_port()
                print(f'optical power meter on port: {self.ser_port}')
            else:
                self.ser = serial.Serial(port=port)
        else:
            self.ser = "dummy"
            self.ser_port = None
            print("connected to dummy optical power meter")
    def determine_port(self):
        """Identifies which serial port of optical power meter if connected."""
        found_correct_port = False
        for serial_port in pyserial_list_ports.comports():
            if serial_port.serial_number == self.serial_detail_serial_number:
                self.ser_port = serial_port
                found_correct_port = True
                print("Tucson 29 on port: "+str(self.ser_port.device))
                break
        if not found_correct_port:
            print("error: could not find power meter")
            self.ser_port = None
    def take_reading(self):
        """Direct the optical power meter to start measuring."""
        if not self.dummy:
            self.initialise_meter_for_readings()
            self.ser.write('DSCLR_A\n'.encode())
            self.ser.write('DSE_A 1\n'.encode())
        self.wait_for_century()
        return self.get_reading()
    def initialise_meter_for_readings(self):
        """Direct the optical power meter to prepare for measuring."""
        self.ser.write('DSE_A 1\n'.encode()) # Enable channel A
        self.ser.write('DSSIZE_A 100\n'.encode()) # Data store buffer size= 100.
        self.ser.write('DSBUF_A 0\n'.encode())
        self.ser.write('LLO 0\n'.encode()) # Local keypad
        self.ser.write(('LAMBDA_A ' + str(self.wavelength) +'\n').encode())
    def request_dscnt_a_and_get_number(self):
        """Data store count query, and return measurement."""
        self.ser.write('DSCNT_A?\n'.encode()) # Data store count query
        return int(self.ser.readline())
    def wait_for_century(self):
        """Wait for 100 samples, to produce a mean and std deviation."""
        if not self.dummy:
            safety_counter = 0
            safety_limit = 1000
            while True and safety_counter < safety_limit:
                number = self.request_dscnt_a_and_get_number()
                print(
                    "["+"#"*int(number/5)+" "*(20-int(number/5))+"] Collection Progress\r"
                    if number % 5 == 0
                    else "",
                    end=""
                )
                if number >= 100:
                    print()
                    break
                safety_counter += 1
        else:
            for number in range(101):
                sleep(0.02)
                print(
                    "["+"#"*int(number/5)+" "*(20-int(number/5))+"] Collection Progress\r"
                    if number % 5 == 0
                    else "",
                    end=""
                )
    def get_reading(self, verbose_print_results=False):
        """Produce a mean and standard deviation measurement"""
        if not self.dummy:
            arr = np.array([])
            raw_data_values = []
            z_sum = 0
            for z_num in range(100):
                z_str = str(z_num+1)
                self.ser.write(('ds_a? '+z_str+'\n').encode())
                raw_data_line = self.ser.readline()
                # To do, if there is a b1, b2, b3 or b4 status, retry
                data_as_cleaned_string = str(raw_data_line).replace("b'0,", "") \
                    .replace("b'1,", "").replace("b'4,", "").replace("\\n", "") \
                    .replace("E", "e").replace("+", "").replace(" ", "") \
                    .replace("'", "")
                data_value_as_float = float(data_as_cleaned_string)
                raw_data_values.append(data_value_as_float)
                if verbose_print_results:
                    print(
                        z_str,
                        raw_data_line,
                        data_as_cleaned_string,
                        data_value_as_float
                    )
                z_sum += data_value_as_float
            # print(arr)
            # print(raw_data_values)
            arr = np.asarray(raw_data_values)
            # print(arr)
            return float(np.mean(arr, axis=0)), float(np.std(arr, axis=0))
        else:
            return float(6.085588080080081e-08), float(5.041437841092559e-09)
    def collect_power_meter_reading(self):
        """Clear power meter buffer, collect 100 samples, and make a reading."""
        self.ser.write('DSCLR_A\n'.encode())
        self.ser.write('DSE_A 1\n'.encode())
        self.wait_for_century()
        self.optical_readings = [] # Added this line to disable accumulation.
        self.optical_readings.append(self.get_reading())
        return ','.join([str(x) for x in self.optical_readings])
