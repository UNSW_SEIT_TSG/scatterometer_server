"""For holding destination command tag objects."""
class DestinationCommandTag:
    """Enable objects holding destination command tag information."""

    def __init__(self, destination, command, tag):
        self.destination = destination
        self.command = command
        self.tag = tag

    def __str__(self):
        return f'{self.destination}::{self.command}::{self.tag}'
