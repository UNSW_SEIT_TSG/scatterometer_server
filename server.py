"""This server relays commands from the Godot GUI to the XPS-C8 and a serial
device for measuring optical power.

It is intended to run in the background on the lab machine or at the very least
to be launched manually prior to opening the GUI interface.
"""

from multiprocessing import Queue, Process
import socket
import time
import serial
import argparse

import python.server.repeated_timer as repeated_timer
import python.server.destination_command_tag as destination_command_tag
import python.server.optical_power_meter as optical_power_meter

import python.server.kbhit as kbhit
from python.server.relay_config import GROUP_MINIMUM_TARGET_POSITIONS as min_targets
from python.server.relay_config import GROUP_MAXIMUM_TARGET_POSITIONS as max_targets
from python.server.relay_config import ADDRESS_PORT as address_port

inbound_gui_address_port = address_port["gui_to_relay"]
outbound_gui_address_port = address_port["relay_to_gui"]
xpsc8_address_port = address_port["xpsc8"]
heartbeat_period = [float(1)]

MODES = ["normal", "test"]

def heart(send_gui_q):
    """Sends a heartbeat signal to gui periodically. Period controlled by timer."""
    send_gui_q.put("--@".encode())


def read_gui(end_q, read_gui_q):
    """Reads data from GUI."""
    sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Make connection with client sending incoming data
    try:
        sck.bind(inbound_gui_address_port)
    except socket.error as error:
        print("01:"+str(error))
    sck.listen(500)
    conn, addr = sck.accept()
    print("02:"+"server connected to client(s<-c): "+addr[0]+";"+str(addr))

    while end_q.empty():
        try:
            data = conn.recv(2048)
            decoded_data = data.decode("utf-8")
            if decoded_data == "!!!":
                end_q.put("!!!")
            else:
                print("03:"+"Received: '"+decoded_data+".")
            read_gui_q.put(data)
            if not data:
                break
        except ConnectionResetError as cre:
            print(cre)
            end_q.put("!!!")
    conn.close()
    print("04:"+"done read_data_read_gui()")
    end_q.put("!!!")


def send_gui(end_q, send_gui_q, heart_q):
    """Sends data to GUI."""
    sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Make connection with client receiving outgoing data
    try:
        sck.bind(outbound_gui_address_port)
    except socket.error as error:
        print("05:"+str(error))
    sck.listen(500)
    conn, addr = sck.accept()
    print("06:"+"server connected to client(s->c): "+addr[0]+";"+str(addr))
    heart_timer = repeated_timer.RepeatedTimer(heartbeat_period[0], heart, send_gui_q)
    heart_timer.start()
    while end_q.empty():
        if not send_gui_q.empty():
            data_decoded = "$$"+send_gui_q.get().decode()+"$$"
            data_encoded = data_decoded.encode()
            conn.sendall(data_encoded)
        if not heart_q.empty():
            heartbeat_period[0] = heart_q.get()
            print(heartbeat_period[0])
            heart_timer.stop()
            heart_timer.__init__(heartbeat_period[0], heart, send_gui_q)
    heart_timer.stop()
    conn.sendall("!!!".encode())
    conn.close()
    print("07:"+"done send_gui()")


def duplex_xpsc8(end_q, send_q, read_q):
    """Function executed as process to relay signals to and from XPS-C8."""
    try:
        sck = socket.socket() # create socket client
        sck.connect(xpsc8_address_port)
        print(f'duplex_xpsc8() connected to xps-c8 on {xpsc8_address_port}')
    except socket.error as error:
        print(f'Connection failure: {error}')
    while end_q.empty():
        if not send_q.empty():
            # Send the message in the queue to the XPS-C8
            to_xps_c8_string = send_q.get()
            print("10:"+to_xps_c8_string.decode())
            sck.sendall(to_xps_c8_string)
            encoded_data = sck.recv(2048)
            decoded_data = encoded_data.decode("utf-8")
            if decoded_data == "!!!":
                end_q.put("!!!")
            else:
                print("11:"+"from xpsc8: '"+decoded_data+".")
            read_q.put(encoded_data)
    sck.close()


def duplex_ser(end_q, send_q, read_q, port):
    """Used by optical power meter process for collecting data via serial port."""
    tucson_29 = optical_power_meter.OpticalPowerMeter(port=port)
    while end_q.empty():
        if not send_q.empty():
            command = send_q.get().decode()
            if command == "GetOpticalPowerMeterReading()":
                read_q.put(tucson_29.collect_power_meter_reading().encode())
            else:
                read_q.put(("Unrecognised command: "+command).encode())
        # if tucson_29.ser.in_waiting > 0:
        #     read_q.put(tucson_29.ser.readline().decode()[: -1].encode())

def dummy_ser(end_q, send_q, read_q):
    """Used in place of duplex_ser when in test mode."""
    while end_q.empty():
        if not send_q.empty():
            command = send_q.get().decode()
            if command == "GetOpticalPowerMeterReading()":
                temporary_reading = float(6.085588080080081e-08), float(5.041437841092559e-09)
                read_q.put(str(temporary_reading).encode())
            else:
                read_q.put(("Unrecognised command: "+command).encode())


def process_inbound_string(inbound_string: str):
    """Looks through inbound_string to structure commands and recipients."""
    result = [] # List of (destination, command) pairs
    commands = inbound_string.split("$$")
    print(commands)
    for destination_command in commands:
        try:
            destination, command, tag = destination_command.split("::")
            result.append(destination_command_tag.DestinationCommandTag(destination, command, tag))
            print(destination, command, tag)
        except ValueError as ve:
            result.append(
                destination_command_tag.DestinationCommandTag(
                    "ValueError", str(ve)+"; input:"+destination_command, "relay"
                )
            )
    return result


def get_fn_name_and_args_from_str(z: str):
    """Return the function name and arguments from a string of the form f(a,b)."""
    if z[-1] == ')':
        function_name, args = z[:-1].split('(')
        args_list = args.split(',')
        return (function_name, args_list)
    return "!!Malformed function!!"


def main_loop(send_gui_q, read_gui_q, send_xpsc8_q, read_xpsc8_q, send_opm_q,
              read_opm_q, heart_q):
    """Manages flow of information between queues & processes."""

    # Read from GUI and send to XPS-C8, Optical Power Meter, or return to GUI.
    if not read_gui_q.empty():
        from_gui_string_encoded = read_gui_q.get()
        from_gui_string_decoded = from_gui_string_encoded.decode()

        # Each message from GUI will be contained between a '$$' and a '$$'"
        # Distribute message according to intended recipient.

        dest_cmd_pairs = process_inbound_string(from_gui_string_decoded)

        for dest_cmd_pair in dest_cmd_pairs:
            if dest_cmd_pair.destination == "opm":
                send_opm_q.put(dest_cmd_pair.command.encode())
                print("received opm command, forwarding to opm process")
            elif dest_cmd_pair.destination == "xpsc8":
                send_xpsc8_q.put(dest_cmd_pair.command.encode())
            elif dest_cmd_pair.destination == "relay":
                # Process instruction intended for relay server itself.
                fn_name, args = get_fn_name_and_args_from_str(dest_cmd_pair.command)
                if fn_name == "GroupMinimumGet":
                    if len(args) == 1:
                        if args[0] == "GROUP1":
                            send_gui_q.put(str(min_targets["GROUP1"]).encode())
                        elif args[0] == "GROUP2":
                            send_gui_q.put(str(min_targets["GROUP2"]).encode())
                        elif args[0] == "GROUP3":
                            send_gui_q.put(str(min_targets["GROUP3"]).encode())
                        elif args[0] == "GROUP4":
                            send_gui_q.put(str(min_targets["GROUP4"]).encode())
                        elif args[0] == "GROUP5":
                            send_gui_q.put(str(min_targets["GROUP5"]).encode())
                        elif args[0] == "GROUP6":
                            send_gui_q.put(str(min_targets["GROUP6"]).encode())
                        elif args[0] == "GROUP7":
                            send_gui_q.put(str(min_targets["GROUP7"]).encode())
                        else:
                            send_gui_q.put(str(
                                "158 Unrecognised argument:"+
                                dest_cmd_pair.command
                            ).encode())
                    else:
                        send_gui_q.put(str(
                            "157 Badly formed function:"+
                            dest_cmd_pair.command
                        ).encode())
                elif fn_name == "GroupMaximumGet":
                    if len(args) == 1:
                        if args[0] == "GROUP1":
                            send_gui_q.put(str(max_targets["GROUP1"]).encode())
                        elif args[0] == "GROUP2":
                            send_gui_q.put(str(max_targets["GROUP2"]).encode())
                        elif args[0] == "GROUP3":
                            send_gui_q.put(str(max_targets["GROUP3"]).encode())
                        elif args[0] == "GROUP4":
                            send_gui_q.put(str(max_targets["GROUP4"]).encode())
                        elif args[0] == "GROUP5":
                            send_gui_q.put(str(max_targets["GROUP5"]).encode())
                        elif args[0] == "GROUP6":
                            send_gui_q.put(str(max_targets["GROUP6"]).encode())
                        elif args[0] == "GROUP7":
                            send_gui_q.put(str(max_targets["GROUP7"]).encode())
                        else:
                            send_gui_q.put(str(
                                "158 Unrecognised argument:"+dest_cmd_pair.command
                            ).encode())
                    else:
                        send_gui_q.put(str(
                            "157 Badly formed function:"+dest_cmd_pair.command
                        ).encode())
                elif fn_name == "SetHeartbeatPeriod":
                    if len(args) == 1:
                        heartbeat_period[0] = float(args[0])
                        print(args[0], float(args[0]), heartbeat_period[0])
                        heart_q.put(heartbeat_period[0])
                    else:
                        send_gui_q.put(str(
                            "218 Badly formed function:"+dest_cmd_pair.command
                        ).encode())
                else:
                    send_gui_q.put(str(
                        "159 Unrecognised function:"+dest_cmd_pair.command
                    ).encode())
            else:
                send_gui_q.put((
                    "162 Unrecognised destination in destination_command_tag:"+
                    str(dest_cmd_pair)
                ).encode())

    # Read from XPS-C8 and send to GUI
    if not read_xpsc8_q.empty():
        from_xpsc8_string_encoded = read_xpsc8_q.get()
        from_xpsc8_decoded = from_xpsc8_string_encoded.decode()
        print(f'Received "{from_xpsc8_decoded}" from XPS-C8, forwarding to GUI."')
        send_gui_q.put(from_xpsc8_string_encoded)

    # Read from optical power meter and send to GUI
    if not read_opm_q.empty():
        from_opm_decoded = read_opm_q.get().decode()
        print(f'Received "{from_opm_decoded}" from Optical Power Meter, forwarding to GUI."')
        send_gui_q.put(from_opm_decoded.encode())


# def main(execution_mode: str = "normal"):
def main(escape_hit, execution_mode: str):
    """Initialisation and control of main_loop."""
    end_q = Queue()

    # Create Queue & Process for Reading Data from GUI via Network
    read_gui_q = Queue()
    read_gui_p = Process(target=read_gui, args=(end_q, read_gui_q))

    # Create Queue for heartbeat control
    heart_q = Queue()

    # Create Queue & Process for Sending Data to GUI via Network
    send_gui_q = Queue()
    send_gui_p = Process(target=send_gui, args=(end_q, send_gui_q, heart_q))

    # Create Queues and Process for XPS-C8 Relay
    send_xpsc8_q = Queue()
    read_xpsc8_q = Queue()
    duplex_xpsc8_p = Process(
        target=duplex_xpsc8, args=(end_q, send_xpsc8_q, read_xpsc8_q)
    )

    # Optical Power Meter Queues & Process
    send_opm_q = Queue()
    read_opm_q = Queue()
   
    if execution_mode == "normal":
        try:
            tucson_29 = optical_power_meter.OpticalPowerMeter()
        except AttributeError as error:
            print("24:"+f"Cannot connect to tucson29. Terminating. {error}")
            return True
    elif execution_mode == "test":
        tucson_29 = optical_power_meter.OpticalPowerMeter("Dummy")
    else:
        # this case is guarded against by the use of the parser choices list
        raise ValueError("error, unexpected execution_mode value")

    duplex_opm_p = Process(
        target=duplex_ser,
        args=(end_q, send_opm_q, read_opm_q, tucson_29.ser_port.device)
    ) if tucson_29.ser_port is not None else Process(
        target=dummy_ser,
        args=(end_q, send_opm_q, read_opm_q)
    )

    # Start Processes if everything is ok at this point
    read_gui_p.start()
    send_gui_p.start()
    duplex_xpsc8_p.start()
    duplex_opm_p.start()

    kb = kbhit.KBHit()
    while end_q.empty():
        main_loop(
            send_gui_q=send_gui_q,
            read_gui_q=read_gui_q,
            send_xpsc8_q=send_xpsc8_q,
            read_xpsc8_q=read_xpsc8_q,
            send_opm_q=send_opm_q,
            read_opm_q=read_opm_q,
            heart_q=heart_q
        )
        if kb.kbhit():
            c = kb.getch()
            if ord(c) == 27:
                escape_hit = True
                print("Escape key press detected.  Program will close ~ 4 seconds.")
                end_q.put("!!!")
                time.sleep(3) # Wait 3 seconds to give processes chance to end.
                read_gui_p.terminate()
                send_gui_p.terminate()
                duplex_xpsc8_p.terminate()
                duplex_opm_p.terminate()
    kb.set_normal_term()
    return escape_hit
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--mode",
        help=f'select one of the following',
        choices=MODES,
        required=True
    )
    args = parser.parse_args()

    outer_escape_hit = False

    print("Hit ESC to close server.")
    while not outer_escape_hit:
        outer_escape_hit = main(outer_escape_hit, args.mode)

