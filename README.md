# Scatterometer_server

A relay server to connect a remote GUI to the various actuator and sensor equipment in the scatterometer lab.

# Setup instructions
The application is intended for use on a windows 10 machine running the anaconda environment manager.

## Anaconda
* Download and install the latest version of anaconda (Python 3.x 64-Bit version) from https://www.anaconda.com/distribution/.
* The most recently tested version at time of writing was https://repo.anaconda.com/archive/Anaconda3-2018.12-Windows-x86_64.exe.
* The installer will prompt to install Microsoft VSCode, it is recommended that this editor be installed.

## Git
* Download and install git from https://git-scm.com/download/win.
* Last tested version was https://github.com/git-for-windows/git/releases/download/v2.21.0.windows.1/Git-2.21.0-64-bit.exe.
* When prompted for default editor used by Git, it is recommended to select: "Use Visual Studio Code as Git's default editor".

## Clone repository to PC
* If an SSH key has not yet been setup go to https://gitlab.com/profile/keys, and https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair.  Follow the instructions to add the SSH key to your gitlab account.
* Launch an Anaconda prompt
* Navigate to the directory where the repository will be cloned ("c:\r" is recommended, e.g. `cd c:\r`).
* Execute the command `git clone git@gitlab.com:UNSW_SEIT_TSG/scatterometer_server.git`
* Enter the cloned repository directory via `cd scatterometer_server`

## Configure Environment
* Create conda environment from yml file by executing the following command from within the Anaconda Prompt: `conda env create -f environment.yml`

## Configure Hardware
* Connect Power Meter to computer so data can be collected during the experiment.

# Launch
* Activate the environment for use, `conda activate scatterometer_server`
* Execute the command: `python server.py --mode=normal` from the root directory of the repository.  Or alternatively, if the optical power meter is not connected, the server can be launched with the command: `python server.py --mode=test`.

# Details
- network name: c260.ad.adfa.edu.au
- expected user name format: AD\z1234567
- last noted ip: 131.236.54.61

# Note
## mono_GPIB_comms not tested in latest review
This module requires a library likely found here https://pyvisa.readthedocs.io/en/master/getting.html but due to equipment access I was not able to validate that it worked correctly.
I've retained the code in the repository for now as it will likely be useful in the future.
